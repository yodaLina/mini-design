package DAO;

import Models.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategRepo extends JpaRepository<Categorie,Long> {
}

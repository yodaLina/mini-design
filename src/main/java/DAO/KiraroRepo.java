package DAO;

import Models.Kiraro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KiraroRepo extends JpaRepository<Kiraro,Long> {
    public void deleteAllByIdcategorie(int idcategorie);
    public void deleteAllByIddestination(int iddestination);
    public List<Kiraro>findByNameContaining(String name);
}

package DAO;

import Models.Kirarodescri;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface KiraroDescriRepo extends JpaRepository<Kirarodescri,Long> {
    public List<Kirarodescri>findAllByIdcategorie(Long idcategorie);
    public List<Kirarodescri>findAllByIdcategorie(Long idcategorie, Pageable pageable);
    public List<Kirarodescri>findByNameContaining(String name,Pageable pageable);
}

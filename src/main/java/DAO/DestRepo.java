package DAO;

import Models.Destination;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Entity;

@Entity
public interface DestRepo extends JpaRepository<Destination,Long>{

}

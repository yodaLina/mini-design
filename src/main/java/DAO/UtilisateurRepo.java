package DAO;

import Models.Utilisateurs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepo extends JpaRepository<Utilisateurs,Long> {
    public Utilisateurs findByNameAndPassword(String name,String password);
}

package DAO;

import Models.Categcount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategCountRepo extends JpaRepository<Categcount,Long> {
}

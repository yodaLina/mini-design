package Services;

import DAO.UtilisateurRepo;
import Models.Utilisateurs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilisateurService {
    @Autowired
    public UtilisateurRepo ur;

    public UtilisateurRepo getUr() {
        return ur;
    }

    public void setUr(UtilisateurRepo ur) {
        this.ur = ur;
    }
    public Long Login(String name,String pass) throws Exception{
        Utilisateurs ul=ur.findByNameAndPassword(name,pass);
        if(ul!=null){
            return ul.getId();
        }
        else{
            throw new Exception("False credentials");
        }
    }
}

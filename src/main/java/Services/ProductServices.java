package Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

@Service
public class ProductServices
{
    @Autowired
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }


    public long getPageNumbers(String texte) throws Exception{
        EntityManager em=null;
        try {
            em=emf.createEntityManager();
            Query q =em.createQuery("Select count(*) from Kiraro where name like '"+texte+"'");
            long valiny=(long)q.getSingleResult();
            if(valiny%4==0){
                return valiny/4;
            }
            else{
                return (valiny/4)+1;

            }
        } catch(Exception e){
            throw e;
        }
        finally{
            if(em!=null){
                em.close();
            }
        }
    }
    public long getPageNumbers(int categ) throws Exception{
        EntityManager em=null;
        try {
            em=emf.createEntityManager();
            Query q =em.createQuery("Select count(*) from Kiraro where idcategorie="+categ);
            long valiny=(long)q.getSingleResult();
            if(valiny%4==0){
                return valiny/4;
            }
            else{
                return (valiny/4)+1;

            }
        } catch(Exception e){
            throw e;
        }
        finally{
            if(em!=null){
                em.close();
            }
        }
    }
    public long getPageNumbers() throws Exception{
        EntityManager em=null;
        try {
            em=emf.createEntityManager();
            Query q =em.createQuery("Select count(*) from Kiraro");
            long valiny=(long)q.getSingleResult();
            if(valiny%4==0){
                return valiny/4;
            }
            else{
                return (valiny/4)+1;

            }
        } catch(Exception e){
           throw e;
        }
        finally{
            if(em!=null){
                em.close();
            }
        }
    }
}

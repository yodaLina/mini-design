package Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Kiraro {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String name;

    public int getIdcategorie() {
        return idcategorie;
    }

    public void setIdcategorie(int idcategorie) {
        this.idcategorie = idcategorie;
    }

    public int getIddestination() {
        return iddestination;
    }

    public void setIddestination(int iddestination) {
        this.iddestination = iddestination;
    }

    private double price;
    private int idcategorie;
    private int iddestination;
    private String shortdescri="";
    private String fulldescri="";
    private String image1;
    private String image2;
    private String image3;
    private int pointure;
    private String couleur;

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)throws Exception{
        if(name==null || name.equals("")){
            throw new Exception("le champ non ne doit pas être nul");
        }
        this.name = name;
    }

    public Kiraro() {
    }

    public Long getId() {
        return id;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) throws Exception {
        if(price<=0){
                throw new Exception("le prix ne doit pas être négatif");
        }
        this.price = price;
    }

    public String getShortdescri() {
        return shortdescri;
    }

    public void setShortdescri(String shortdescri) throws Exception {
        if(shortdescri==null || shortdescri.equals("")){
            throw new Exception("le champ shortdescription ne doit pas être vide");
        }
        this.shortdescri = shortdescri;
    }

    public String getFulldescri() {
        return fulldescri;
    }

    public void setFulldescri(String fulldescri) throws Exception
    {
        if(fulldescri==null || fulldescri.equals("")){
            throw new Exception("le champ fulldescri ne doit pas être vide");
        }
        this.fulldescri = fulldescri;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public int getPointure() {
        return pointure;
    }

    public void setPointure(int pointure) throws Exception{
        if(pointure<=0){
            throw new Exception("La pointure ne doit pas être négative ou nulle");
        }
        this.pointure = pointure;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) throws Exception{
        if(couleur==null || couleur.equals("")){
            throw new Exception("le champ couleur ne doit pas être vide");
        }
        this.couleur = couleur;
    }
}

package Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Categorie {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Categorie() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception{
        if(name==null || name.equals("")){
            throw new Exception("Le champ nom ne doit pas être vide");
        }
        this.name = name;
    }

    public Categorie(String name)throws Exception {
        this.setName(name);
    }
}

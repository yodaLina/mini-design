package Models;

import javax.persistence.*;

@Entity
@Table(name="Destination")
public class Destination {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Destination() {
    }

    public Destination(String name) {

        this.name = name;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if(name==null || name.equals("")){
            throw new Exception("Le champ nom doit être rempli");
        }
        this.name = name;
    }
}

package Models;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class Categcount {
    @Id
    private Long id;
    private String name;
    private int isany;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getIsany() {
        return isany;
    }

    public Categcount() {

    }

    public Categcount(Long id, String name, int isany) {

        this.id = id;
        this.name = name;
        this.isany = isany;
    }
}

package Models;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Immutable
public class Kirarodescri{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    public Kirarodescri() {
    }

    private String name;
    private double price;
    private Long idcategorie;
    private Long iddestination;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getIdcategorie() {
        return idcategorie;
    }

    public void setIdcategorie(Long idcategorie) {
        this.idcategorie = idcategorie;
    }

    public Long getIddestination() {
        return iddestination;
    }

    public void setIddestination(Long iddestination) {
        this.iddestination = iddestination;
    }

    public String getShortdescri() {
        return shortdescri;
    }

    public void setShortdescri(String shortdescri) {
        this.shortdescri = shortdescri;
    }

    public String getFulldescri() {
        return fulldescri;
    }

    public void setFulldescri(String fulldescri) {
        this.fulldescri = fulldescri;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public int getPointure() {
        return pointure;
    }

    public void setPointure(int pointure) {
        this.pointure = pointure;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getCategname() {
        return categname;
    }

    public void setCategname(String categname) {
        this.categname = categname;
    }

    public String getDestname() {
        return destname;
    }

    public void setDestname(String destname) {
        this.destname = destname;
    }

    public Kirarodescri(String name, double price, Long idcategorie, Long iddestination, String shortdescri, String fulldescri, String image1, String image2, String image3, int pointure, String couleur, String categname, String destname) {

        this.name = name;
        this.price = price;
        this.idcategorie = idcategorie;
        this.iddestination = iddestination;
        this.shortdescri = shortdescri;
        this.fulldescri = fulldescri;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.pointure = pointure;
        this.couleur = couleur;
        this.categname = categname;
        this.destname = destname;
    }

    private String shortdescri;
    private String fulldescri;
    private String image1;
    private String image2;
    private String image3;
    private int pointure;
    private String couleur;
    private String categname;
    private String destname;

}

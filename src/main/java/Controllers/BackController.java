package Controllers;

import DAO.CategRepo;
import DAO.DestRepo;
import DAO.KiraroDescriRepo;
import DAO.KiraroRepo;
import Models.*;
import Services.UtilisateurService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("/BackOffice")
public class BackController {
    @Autowired
    private UtilisateurService ur;
    @Autowired
    private CategRepo cr;
    @Autowired
    private KiraroDescriRepo kdr;
    @Autowired
    private DestRepo dr;

    @Autowired
    private KiraroRepo krp;
    public boolean testLogin(HttpServletRequest request){
        return (request.getSession().getAttribute("log")!=null);
    }
    public void redirectLogin(HttpServletRequest request) throws Exception{
        if(!testLogin(request)){
            throw new Exception("");
        }
    }
    public KiraroRepo getKrp() {
        return krp;
    }

    public void setKrp(KiraroRepo krp) {
        this.krp = krp;
    }

    public void setCr(CategRepo cr) {
        this.cr = cr;
    }

    public void setDr(DestRepo dr) {
        this.dr = dr;
    }

    public void setUr(UtilisateurService ur) {
        this.ur = ur;
    }

    @GetMapping("/")
    public String index(){
        return "redirect:/BackOffice/Login";
    }

    @GetMapping("/Login")
    public String index2(HttpServletRequest request,@RequestParam(required =false)String message,Model m){
        if(testLogin(request)){
            return "redirect:/BackOffice/Produits";
        }
        m.addAttribute("message",message);
        return "BackOffice/index";
    }
    @GetMapping("/Destinations")
    public String Destinations(HttpServletRequest request,@ModelAttribute("dest")Destination dest, Model md){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "/BackOffice/";
        }
        md.addAttribute("liste",dr.findAll());
        return "BackOffice/Destinations";
    }
    @PostMapping("/Destinations/Creation")
    public String createDest(HttpServletRequest request,RedirectAttributes ra, @ModelAttribute("dest") @Valid Destination dest,BindingResult br,Model m){
        String message="Success";
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "/BackOffice/";
        }
        if(br.hasErrors()){
            m.addAttribute("liste",dr.findAll());
            return "BackOffice/Destinations";
        }
        else{
            try {
                dr.save(dest);
            }
            catch (Exception e){
                message=e.getMessage();
            }
            finally{
                System.out.println("ato");
                ra.addAttribute("message",message);
                return "redirect:/BackOffice/Destinations";
            }
        }
    }
    @PostMapping("/Categories/Creation")
    public String createCateg(HttpServletRequest request,RedirectAttributes ra, @ModelAttribute("categ") @Valid Categorie categ,BindingResult br,Model m){
        String message="Success";
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "/BackOffice/";
        }
        if(br.hasErrors()){
            m.addAttribute("liste",cr.findAll());
            return "BackOffice/Categories";
        }
        else{
            try {
                cr.save(categ);
            }
            catch (Exception e){
                message=e.getMessage();
            }
            finally{
                System.out.println("ato");
                ra.addAttribute("message",message);
                return "redirect:/BackOffice/Categories";
            }
        }
    }
    @PostMapping("/Produits/Creation")
    public String createCateg(HttpServletRequest request,RedirectAttributes ra, @ModelAttribute("prod") @Valid Kiraro prod,BindingResult br,Model m){
        String message="Success";
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        if(br.hasErrors()){
            m.addAttribute("liste", krp.findAll());
            return "BackOffice/Produits";
        }
        else{
            try {
                krp.save(prod);
            }
            catch (Exception e){
                message=e.getMessage();
            }
            finally{
                System.out.println("ato");
                ra.addAttribute("message",message);
                return "redirect:/BackOffice/Produits";
            }
        }
    }
    @PostMapping("/Produits/Update")
    public String UpdateProducts(HttpServletRequest request,RedirectAttributes rda,@RequestBody String id){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        Long idl=Long.valueOf(id.split("=")[1]);
        rda.addAttribute("id",idl);
        return "redirect:/BackOffice/Produits";
    }
    private static String getFileExtension(File file) {

        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }
    @PostMapping("/Produits/Delete")
    public String deleteproduct(HttpServletRequest request,@RequestBody String id){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        Long idl=Long.valueOf(id.split("=")[1]);
        krp.deleteById(idl);
        return "redirect:/BackOffice/Produits";
    }
    @PostMapping("/Produits/ChangeImage/{id}/{numero}")
    public String change(@RequestParam("fichier")MultipartFile file,@PathVariable("id")Long id,@PathVariable("numero")Long numero,RedirectAttributes ra,HttpServletRequest request){
            String message="Success";
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
            try{
                if (file.isEmpty() || (!file.getName().endsWith(".jpg")) && (!file.getName().endsWith(".png"))){
                    message="Please select a valid file to upload";
                }
                byte[] bytes = file.getBytes();
                Path path = Paths.get(request.getServletContext().getRealPath("")+"..\\resources\\static\\img\\product\\"+file.getOriginalFilename());
                Files.write(path, bytes);
                Kiraro k=krp.findById(id).get();
                        if(numero==1){
                            k.setImage1(file.getOriginalFilename());
                        }
                        else if(numero==2) {
                            k.setImage2(file.getOriginalFilename());
                        }
                        else if(numero==3) {
                            k.setImage3(file.getOriginalFilename());
                        }
                krp.save(k);

            }
            catch (Exception e)
            {
                e.printStackTrace();
                message=e.getMessage();
            }
            finally{
                ra.addFlashAttribute("message",message);
                return "redirect:/BackOffice/Produits?id="+id;
            }
    }
    @PostMapping("/Categories/Delete")
    public String deletecateg(HttpServletRequest request,@RequestBody String id){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        String valiny=id.split("=")[1];
        krp.deleteAllByIdcategorie(Integer.valueOf(valiny));
        cr.deleteById(Long.valueOf(valiny));
        return "redirect:/BackOffice/Categories";
    }
    @GetMapping("/Recherche")
    public String rechercher(HttpServletRequest request,@ModelAttribute("prod") Kiraro k,Model md,@RequestParam String texte){
        if(texte==null){
            texte="";
        }
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        md.addAttribute("listec",cr.findAll());
        md.addAttribute("listed",dr.findAll());
        md.addAttribute("liste",krp.findByNameContaining(texte));
        return "BackOffice/Produits";
    }
    @PostMapping("/Destinations/Delete")
    public String deletedest(HttpServletRequest request,@RequestBody String id){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        String valiny=id.split("=")[1];
        krp.deleteAllByIddestination(Integer.valueOf(valiny));
        dr.deleteById(Long.valueOf(valiny));
        return "redirect:/BackOffice/Destinations";
    }
    @GetMapping("/Produits")
    public String Produits(HttpServletRequest request,@ModelAttribute("prod") Kiraro k,@ModelAttribute("idupdate")String idupdate,Model md,@RequestParam(name="id",required=false)Long id){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        if(id!=null){
            k=krp.findById(Long.valueOf(id)).get();
            md.addAttribute("prod",k);
        }
        md.addAttribute("listec",cr.findAll());
        md.addAttribute("listed",dr.findAll());
        md.addAttribute("liste",kdr.findAll());
        return "BackOffice/Produits";
    }
    @GetMapping("/Categories")
    public String Categories(HttpServletRequest request,@ModelAttribute("categ") Categorie categ,@RequestParam(required=false)String message ,Model md){
        try{
            redirectLogin(request);
        }
        catch (Exception e){
            return "redirect:/BackOffice/";
        }
        md.addAttribute("liste",cr.findAll());
        return "BackOffice/Categories";
    }
    @PostMapping("/Log")
    public String Login(HttpServletRequest request,RedirectAttributes ra){
            String message="";
            if(testLogin(request)){
                return "redirect:/BackOffice/Produits";
            }
            try{
                Utilisateurs ul=new Utilisateurs(Long.valueOf("0"),request.getParameter("name"),request.getParameter("pass"));
                Long id=ur.Login(request.getParameter("name"),request.getParameter("pass"));
                request.getSession().setAttribute("log",id);
                ra.addAttribute("message",message);
                return "redirect:/BackOffice/Categories";
            }
            catch(Exception e){
                message=e.getMessage();
                ra.addAttribute("message",message);
                return "redirect:/BackOffice/";
            }
    }

    @PostMapping(value = "/Inscrire")
    public String Inscription(HttpServletRequest request,RedirectAttributes ra){
        Utilisateurs ux=new Utilisateurs();
        String message="Success";
        if(testLogin(request)){
            return "redirect:/BackOffice/Produits";
        }
        try{
            ux.setName(request.getParameter("name"));
            ux.setPassword(request.getParameter("pass"));
            ur.getUr().save(ux);
        }
        catch(Exception e){
            message=e.getMessage();
        }
        finally{
            ra.addAttribute("message",message);
            return "redirect:/BackOffice/Inscription";
        }
    }
    @GetMapping("/Inscription")
    public String Inscription(HttpServletRequest request,@RequestParam(required = false) String message,Model m){
        if(testLogin(request)){
            return "redirect:/BackOffice/Produits";
        }
        m.addAttribute("message",message);
        return "BackOffice/Inscription";
    }
}

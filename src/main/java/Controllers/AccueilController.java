package Controllers;

import DAO.CategCountRepo;
import DAO.KiraroDescriRepo;
import DAO.KiraroRepo;
import Models.Categcount;
import Models.Categorie;
import Models.Kirarodescri;
import Services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class AccueilController {
    @Autowired
    private ProductServices ps;
    @Autowired
    private CategCountRepo ccr;
    @Autowired
    private KiraroDescriRepo kr;

    public void setKr(KiraroDescriRepo kr) {
        this.kr = kr;
    }

    public void setCcr(CategCountRepo ccr) {
        this.ccr = ccr;
    }

    public void setPs(ProductServices ps) {
        this.ps = ps;
    }

    @GetMapping("/")
    public String baseredir()
    {
        return "redirect:/Liste-des-produits-Categorie-All-Page-0-CAll-All";
    }

    @GetMapping("/Recherche")
    public String recherche(@RequestParam("texte")String texte){
        if(texte.equals("")){
            texte="All";
        }
        return "redirect:Liste-des-produits-Categorie-All-Page-0-CAll-"+texte+"";
    }
    @GetMapping("/Liste-des-produits/{recherche}/{categorie}/{page}")
    public String ListeProduits(@PathVariable("recherche")String rech,@PathVariable("categorie")String categorie,@PathVariable("page")Long number , Map<String,Object>model) throws Exception{
        long size=ps.getPageNumbers();
        String title="";
        String categname="";
        List<Kirarodescri>liste=null;
        model.put("rech","All");
        if(!rech.equals("All")){
            title="Liste des produits";
            liste=kr.findByNameContaining(rech,PageRequest.of(number.intValue(),4).toOptional().get());
            model.put("Categorie","All");
            model.put("categname","All");
            model.put("rech",rech);
            size=ps.getPageNumbers("%"+rech+"%");
        }
        else if(categorie.equals("All")){
            title="Liste des produits";
            liste=kr.findAll(PageRequest.of(number.intValue(),4)).getContent();
            model.put("Categorie","All");
            model.put("categname","All");
        }
        else{
            Long valeur=Long.valueOf(categorie);
            Categcount c=ccr.findById(valeur).get();
            size=ps.getPageNumbers(valeur.intValue());
            liste=kr.findAllByIdcategorie(valeur,PageRequest.of(number.intValue(),4).toOptional().get());
            title="Categorie "+c.getName();
            model.put("Categorie",c.getId());
            model.put("categname",c.getName());
        }
        model.put("Titre",title);
        model.put("Categories",ccr.findAll());
        model.put("Products",liste);
        model.put("pages",size);
        model.put("currentpage",number);
        return "index";
    }
    @GetMapping("/Produit/{idproduit}")
    public String single(@PathVariable("idproduit")Long idproduit,Map<String,Object>model)
    {
        Kirarodescri kd=kr.findById(idproduit).get();
        model.put("Titre",kd.getName());
        model.put("single",kd);
        return "single";
    }


}

package SpringRunner;

import Services.CustomURLRewriter;
import Services.FileStorageProperties;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
@ComponentScan(basePackages = {"Controllers","Services","DAO"})
@EnableJpaRepositories(basePackages = "DAO")
@EntityScan(basePackages = {"Models"})
@EnableTransactionManagement
public class SpringRun{

    public static void main(String[]args){
        //blablaxsqdsqqsdqs
        String s="";
        SpringApplication.run(SpringRun.class,args);
    }
    @Bean
    public FilterRegistrationBean tuckeyRegistrationBean() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new CustomURLRewriter());
        return registrationBean;
    }
}

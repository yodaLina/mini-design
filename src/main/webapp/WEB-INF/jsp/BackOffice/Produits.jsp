<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gestion de produits">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Produits</title>
    <style>
        input[type~="number"]{
            text-align: right;
        }
    </style>
    <!-- Fontfaces CSS-->
    <!--link href="/Back/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all"-->

    <!-- Main CSS-->
    <link href="/Back/css/theme.css" rel="stylesheet" media="all">
    <link href="/Back/css/font-face.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/prod/Full.css" rel="stylesheet" media="all">
    <script src="/Back/js/ckeditor.js"></script>
</head>

<body class="animsition">
<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <%@include file="Menu.jsp"%>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <%@include file="Header.jsp"%>
        <!-- END HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <h1>Produits</h1><br><br>
                <div class="container-fluid" style="background-color:#f9f9ff">
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                            <form:form action="/BackOffice/Produits/Creation" method="POST" style="margin:50px;" modelAttribute="prod">
                                <div class="form-title">
                                    <h3>Nouveau Produit</h3><br><br>
                                </div>
                                <div>
                                    <div class="row form-group">
                                        <form:input id="id" type="hidden" class="form-control" path="id" />
                                            <form:label for="name" class="minititle" path="name">Name</form:label>
                                            <form:input id="name" value="${prod.getName()}" class="form-control" path="name" />
                                            <form:label for="price"  class="minititle" path="price">Price</form:label>
                                            <form:input id="price" value="${prod.getPrice()}" type="number" class="form-control" path="price" /><br><br>
                                        <div class="form-group">
                                            <form:label style="width:100%" for="idcategorie"  class="minititle" path="idcategorie">Catégorie</form:label>
                                            <form:select style="width:100%" path="idcategorie">
                                                <c:forEach var="item" items="${listec}">
                                                    <form:option value="${item.getId()}">${item.getName()}</form:option>
                                                </c:forEach>
                                            </form:select>
                                        </div>
                                        <div class="form-group">
                                            <form:label style="width:100%" for="iddestination"  class="minititle" path="iddestination">Pour les</form:label>
                                            <form:select style="width:100%" path="iddestination">
                                                <c:forEach var="item" items="${listed}">
                                                    <form:option value="${item.getId()}">${item.getName()}</form:option>
                                                </c:forEach>
                                            </form:select>
                                        </div>
                                            <form:label style="width:100%" for="pointure"  class="minititle" path="pointure">Pointure</form:label>
                                            <form:input value="${prod.getPointure()}" id="pointure" type="number" class="form-control" path="pointure" />
                                            <form:label for="couleur" class="minititle" path="couleur">Couleur</form:label>
                                            <form:input value="${prod.getCouleur()}" id="couleur" class="form-control" path="couleur" />
                                            <form:label style="margin-right:25%;" class="minititle" path="shortdescri">Description courte</form:label></br></br>
                                            <form:textarea style="float:right;" id="editor1"  class="form-control" path="shortdescri" value="${prod.getShortdescri()}"></form:textarea>
                                            </br></br>
                                            <form:label style="margin-right:25%;" class="minititle" path="fulldescri">Description</form:label></br></br>
                                            <form:textarea style="float:right" id="editor2"  class="form-control" path="fulldescri" value="${prod.getFulldescri()}"></form:textarea>
                                            </br></br>
                                    </div>
                                </div>
                                <div class="row">
                                    <h2></h2>
                                </div>
                                <div class="row form-group">
                                    <button class="btn btn-outline-primary btn-lg">Confirmer</button>
                                    <form:errors path="*" element = "h6" cssClass="alert-danger" />
                                </div>
                            </form:form>
                            </center>
                        </div>
                    </div>
                    <c:if test="${not empty prod.getId()}">
                    <div class="row" style="text-align:center;">
                        <h2 style="text-decoration:underline;width:100%;margin-bottom:2em;"">Gallerie pour le produit</h2>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header" style="background-color:#00a2e3">
                                        <h3 style="color:white;">Image descriptive principale</h3>
                                    </div>
                                    <div class="card-body">
                                        <img class="img-rounded" src="/img/product/${prod.getImage1()}" alt="${prod.getName()}">
                                        <form method="post" style="padding:5%;" action="/BackOffice/Produits/ChangeImage/${prod.getId()}/1" enctype="multipart/form-data">
                                            <input id="fichier" name="fichier" type="file" />
                                            <button style="margin-top:1em;margin-bottom:1em;" class="btn btn-outline-primary"><i class="fa fa-upload"> </i>Upload</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header" style="background-color:#00a2e3">
                                        <h3 style="color:white;">Image descriptive primaire</h3>
                                    </div>
                                    <div class="card-body">
                                        <img class="img-rounded" src="/img/product/${prod.getImage2()}" alt="${prod.getName()}">
                                        <form method="post" style="padding:5%;" action="/BackOffice/Produits/ChangeImage/${prod.getId()}/2" enctype="multipart/form-data">
                                            <input id="fichier" name="fichier" type="file" />
                                            <button style="margin-top:1em;margin-bottom:1em;" class="btn btn-outline-primary"><i class="fa fa-upload"> </i>Upload</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header" style="background-color:#00a2e3">
                                        <h3 style="color:white;">Image descriptive secondaire</h3>
                                    </div>
                                    <div class="card-body">
                                        <img class="img-rounded" src="/img/product/${prod.getImage3()}" alt="${prod.getName()}">
                                        <form method="post" style="padding:5%;" action="/BackOffice/Produits/ChangeImage/${prod.getId()}/3" enctype="multipart/form-data">
                                            <input id="fichier" name="fichier" type="file" />
                                            <button style="margin-top:1em;margin-bottom:1em;" class="btn btn-outline-primary"><i class="fa fa-upload"> </i>Upload</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <c:if test="${not empty message}">


                            <div class="col-sm-12">
                                <div class="alert-info">
                                        <h4>${message}</h4>
                                </div>
                            </div>
                            </c:if>
                    </div>
                    </c:if>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE -->
                            <h2>Produits</h2><br><br>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>Name</th>
                                        <th>Couleur</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${liste}">
                                        <tr class="tr-shadow">
                                            <td>
                                            </td>
                                            <td>${item.getName()}</td>
                                            <td>${item.getCouleur()}</td>
                                            <td>
                                                <form action="/BackOffice/Produits/Delete" method="post">
                                                    <input type="hidden" name="id" value="${item.getId()}"/>
                                                    <button class="btn btn-outline-danger"><i class="fa fa-remove"></i></button>
                                                </form>
                                                <form style="float:left;" action="/BackOffice/Produits/Update" method="post">
                                                    <input type="hidden" name="id" value="${item.getId()}"/>
                                                    <button class="btn btn-outline-primary"><i class="fa fa-level-up-alt">U</i></button>

                                                </form>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Jquery JS-->

<script src="/Back/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="/Back/vendor/prod/popper.min.js"></script>
<script src="/Back/vendor/prod/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="/Back/vendor/prod/wow.min.js"></script>
<script src="/Back/vendor/prod/animsition.min.js"></script>
<script src="/Back/vendor/prod/bootstrap-progressbar.min.js">
</script>
<script src="/Back/vendor/prod/jquery.waypoints.min.js"></script>
<script src="/Back/vendor/prod/jquery.counterup.min.js">
</script>
<script src="/Back/vendor/prod/select2.min.js">
</script>

<!-- Main JS-->
<script src="/Back/js/main.js"></script>

<!-- Main JS-->
</body>
<script>
    CKEDITOR.replace( 'editor1',{
        width:['100%']
    });
    CKEDITOR.replace( 'editor2',{
        width:['100%']
    });
</script>
</html>
<!-- end document-->

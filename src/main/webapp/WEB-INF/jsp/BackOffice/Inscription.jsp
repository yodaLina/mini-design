<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Inscription">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Inscription</title>

    <!-- Fontfaces CSS-->
    <!--link href="/Back/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all"-->

    <!-- Main CSS-->
    <link href="/Back/css/theme.css" rel="stylesheet" media="all">
    <link href="/Back/css/font-face.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/prod/Full.css" rel="stylesheet" media="all">
</head>

<body class="animsition">
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <div class="bg-logo2"></div>
                        </a>
                    </div>
                    <div class="login-form">
                        <h1><center>Inscription</center></h1>
                        <form action="/BackOffice/Inscrire" method="post">
                            <div class="form-group">
                                <label>Username</label>
                                <input class="au-input au-input--full" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="pass" placeholder="Password">
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                        </form>

                        <div class="register-link">
                            <c:if test="${message!=null}">
                                <h4 class="alert-info">
                                        ${message}
                                </h4>
                            </c:if>
                            <p>
                                <a href="/BackOffice/Login">Login</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Jquery JS-->
<script src="/Back/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="/Back/vendor/prod/popper.min.js"></script>
<script src="/Back/vendor/prod/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="/Back/vendor/prod/wow.min.js"></script>
<script src="/Back/vendor/prod/animsition.min.js"></script>
<script src="/Back/vendor/prod/bootstrap-progressbar.min.js">
</script>
<script src="/Back/vendor/prod/jquery.waypoints.min.js"></script>
<script src="/Back/vendor/prod/jquery.counterup.min.js">
</script>
<script src="/Back/vendor/prod/select2.min.js">
</script>

<!-- Main JS-->
<script src="/Back/js/main.js"></script>

</body>

</html>
<!-- end document-->
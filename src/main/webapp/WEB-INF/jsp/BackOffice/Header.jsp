<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap">
                <form class="form-header" action="/BackOffice/Recherche" method="GET">
                    <input class="au-input au-input--xl" type="text" name="texte" placeholder="Search for products" />
                    <button class="au-btn--submit" type="submit">
                        <i class="zmdi zmdi-search"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</header>
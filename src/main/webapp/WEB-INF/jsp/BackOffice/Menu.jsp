<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.html">
                    <img src="/Back/images/icon/logo.png" alt="CoolAdmin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li>
                    <a href="/BackOffice/Categories">
                        <i class="fas fa-angle-double-left"></i>Categories</a>
                </li>
                <li>
                    <a href="/BackOffice/Destinations">
                        <i class="fas fa-pen-square"></i>Destinations</a>
                </li>
                <li>
                    <a href="/BackOffice/Produits">
                        <i class="fas fa-shopping-cart"></i>Produits</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->

<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="/Back/images/icon/logo.png" alt="Cool Admin"/>
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li>
                    <a href="/BackOffice/Categories">
                        <i class="fas fa-angle-double-left"></i>Categories</a>
                </li>
                <li>
                    <a href="/BackOffice/Destinations">
                        <i class="fas fa-pen-square"></i>Destinations</a>
                </li>
                <li>
                    <a href="/BackOffice/Produits">
                        <i class="fas fa-shopping-cart"></i>Produits</a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
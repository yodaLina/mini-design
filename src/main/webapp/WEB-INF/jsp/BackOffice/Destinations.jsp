<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gestion des Destinations">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Destinations</title>

    <!-- Fontfaces CSS-->

    <!--link href="/Back/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all"-->

    <!-- Main CSS-->
    <link href="/Back/css/theme.css" rel="stylesheet" media="all">
    <link href="/Back/css/font-face.css" rel="stylesheet" media="all">
    <link href="/Back/vendor/prod/Full.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <%@include file="Menu.jsp"%>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <%@include file="Header.jsp"%>
        <!-- END HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <h1>Destinations</h1><br><br>
                <div class="container-fluid" style="background-color:#f9f9ff">

                    <div class="row">
                        <div class="col-md-4">
                            <form:form action="/BackOffice/Destinations/Creation" method="POST" style="margin:50px;" modelAttribute="dest">
                                <div class="form-title">
                                    <h3>Nouvelle Destination</h3><br><br>
                                </div>
                                <div>
                                    <div class="row form-group">
                                        <form:input id="id" type="hidden" class="form-control" path="id" />
                                        <form:label for="name" class="minititle" path="name">Name</form:label>
                                        <br><br>
                                        <form:input id="name" class="form-control" path="name" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <button class="btn btn-outline-primary btn-lg">Confirmer</button>
                                    <form:errors path="*" element = "h6" cssClass="alert-danger" />
                                </div>
                            </form:form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE -->

                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                    <tr>

                                        <th>Name</th>
                                        <th>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${liste}">
                                        <tr class="tr-shadow">
                                            <td>${item.getName()}</td>
                                            <td>
                                            <form action="/BackOffice/Destinations/Delete" method="post">
                                                <input type="hidden" name="id" value="${item.getId()}"/>
                                                <button class="btn btn-outline-danger"><i class="fa fa-remove"></i></button>
                                            </form>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Jquery JS-->
<script src="/Back/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="/Back/vendor/prod/popper.min.js"></script>
<script src="/Back/vendor/prod/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="/Back/vendor/prod/wow.min.js"></script>
<script src="/Back/vendor/prod/animsition.min.js"></script>
<script src="/Back/vendor/prod/bootstrap-progressbar.min.js">
</script>
<script src="/Back/vendor/prod/jquery.waypoints.min.js"></script>
<script src="/Back/vendor/prod/jquery.counterup.min.js">
</script>
<script src="/Back/vendor/prod/select2.min.js">
</script>

<!-- Main JS-->
<script src="/Back/js/main.js"></script>

</body>

</html>
<!-- end document-->


<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Ravelojaona Tokiniaina Mathias">
	<!-- Meta Description -->
	<meta name="description" content="${Titre}">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>${Titre}</title>

	<!--
            CSS
            ============================================= -->
	<!--link rel="stylesheet" href="/css/linearicons.css">
	<link rel="stylesheet" href="/css/owl.carousel.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/nice-select.css">
	<link rel="stylesheet" href="/css/nouislider.min.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/Sprites.css"-->
	<link rel="stylesheet" href="/css/Styles.css">
</head>

<body id="category">

	<!-- Start Header Area -->
	<!-- End Header Area -->
	<%@ include file="Header.jsp"%>
	<!-- Start Banner Area -->

	<!-- End Banner Area -->
	<div class="container">
		<div class="row">
			<%@ include file="Menu.jsp"%>
			<div class="col-xl-9 col-lg-8 col-md-7">
				<!-- Start Filter Bar -->
				<c:if test="${pages!=0}">
				<div class="filter-bar d-flex flex-wrap align-items-center">
					<div class="pagination">
						<c:forEach var="i" begin="0" end="${pages-1}">
							<a href="/Liste-des-produits-Categorie-${categname}-Page-${i}-C${Categorie}-${rech}" class="active">${i}</a>
						</c:forEach>
					</div>
				</div>
				</c:if>
				<!-- End Filter Bar -->
				<!-- Start Best Seller -->
				<section class="lattest-product-area pb-40 category-list">
					<div class="row">
						<!-- single product -->
						<c:forEach var="item" items="${Products}">
						<div class="col-lg-4 col-md-6">
							<div class="single-product">
								<img class="img-fluid" src="/img/product/${item.getImage1()}" alt="${item.getShortdescri()}">
								<div class="product-details">
									<h6>${item.getName()}</h6>
									<div class="price">
										<h6>$${item.getPrice()}</h6>
									</div>
									<div class="prd-bottom">
										<a href="/Produit/${item.getName().replaceAll(" ","-")}_P${item.getId()}" class="social-info">
											<span class="lnr lnr-move"></span>
											<p class="hover-text">view more</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						</c:forEach>
						<!-- single product -->
						<!-- single product -->
						<!-- single product -->
						<!-- single product -->
						<!-- single product -->
					</div>
				</section>
				<!-- End Best Seller -->
				<!-- Start Filter Bar -->
				<c:if test="${pages!=0}">
					<div class="filter-bar d-flex flex-wrap align-items-center">
						<div class="pagination">
							<c:forEach var="i" begin="0" end="${pages-1}">
								<a href="/Liste-des-produits-Categorie-${categname}-Page-${i}-C${Categorie}-${rech}" class="active">${i}</a>
							</c:forEach>
						</div>
					</div>
				</c:if>
				<!-- End Filter Bar -->
			</div>
		</div>
	</div>

	<!-- Start related-product Area -->
	<!-- End related-product Area -->

	<!-- start footer Area -->
		<%@include file="Footer.jsp"%>
	<!-- End footer Area -->

	<!-- Modal Quick Product View -->



	<script src="/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<script src="/js/jquery.ajaxchimp.min.js"></script>
	<script src="/js/jquery.nice-select.min.js"></script>
	<script src="/js/jquery.sticky.js"></script>
	<script src="/js/jquery.magnific-popup.min.js"></script>
	<script src="/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="/js/main.js"></script>
</body>

</html>
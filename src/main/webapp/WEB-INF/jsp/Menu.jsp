<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xl-3 col-lg-4 col-md-5">
    <div class="sidebar-categories">
        <div class="head">Browse Categories</div>
        <c:forEach var="item" items="${Categories}">
            <ul class="main-categories" style="width:100%;">
                <li class="main-nav-list" >
                    <a href="/Liste-des-produits/Categorie_${item.getName()}_C${item.getId()}" >
                        <span class="lnr lnr-arrow-right"></span>${item.getName()}<span class="number" style="float:right;">(${item.getIsany()})</span></a>
                </li>
            </ul>
        </c:forEach>
    </div>
</div>
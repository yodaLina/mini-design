<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="/img/fav.png">
	<!-- Author Meta -->
    <meta name="author" content="Ravelojaona Tokiniaina Mathias">
    <!-- Meta Description -->
    <meta name="description" content="${Titre}">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>${Titre}</title>
	<!--
			CSS
			============================================= -->
	<!--link rel="stylesheet" href="/css/linearicons.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/themify-icons.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/css/owl.carousel.css">
	<link rel="stylesheet" href="/css/nice-select.css">
	<link rel="stylesheet" href="/css/nouislider.min.css">
	<link rel="stylesheet" href="/css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="/css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/Sprites.css"-->
	<link rel="stylesheet" href="/css/Styles.css">
</head>

<body>

	<!-- Start Header Area -->
	<%@include file="Header.jsp"%>
	<!-- End Banner Area -->

	<!--================Single Product Area =================-->
	<div class="product_image_area">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid" src="/img/product/${single.getImage2()}" alt="${single.getShortdescri()}-image primaire">
						</div>
						<div class="single-prd-item">
							<img class="img-fluid" src="/img/product/${single.getImage3()}" alt="${single.getShortdescri()}-image secondaire">
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h3>${single.getName()}</h3>
						<h2>$${single.getPrice()}</h2>
						<ul class="list">
							<li><a class="active" href="/#"><span>Category</span> : ${single.getCategname()}</a></li>
							<li><a href="/#"><span>For : </span>${single.getDestname()}</a></li>
						</ul><br><br>
						<p>${single.getShortdescri()}</p>
					</div>

				</div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <h5>Pointure</h5>
                            </td>
                            <td>
                                <h5>${single.getPointure()}</h5>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h5>Couleur</h5>
                            </td>
                            <td>
                                <h5>${single.getCouleur()}</h5>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" style="margin-bottom: 4em;margin-top: 2em;">
                    <h5 style="margin-bottom:3em;"><center>Description</center></h5>
                    ${single.getFulldescri()}
                </div>
			</div>
		</div>
	</div>
	<!--================End Single Product Area =================-->

	<!--================Product Description Area =================-->
	<!--================End Product Description Area =================-->

	<!-- Start related-product Area -->
	<!-- End related-product Area -->

	<!-- start footer Area -->
	<%@include file="Footer.jsp"%>
	<!-- End footer Area -->

	<script src="/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="/js/vendor/bootstrap.min.js"></script>
	<script src="/js/jquery.ajaxchimp.min.js"></script>
	<script src="/js/jquery.nice-select.min.js"></script>
	<script src="/js/jquery.sticky.js"></script>
	<script src="/js/nouislider.min.js"></script>
	<script src="/js/jquery.magnific-popup.min.js"></script>
	<script src="/js/owl.carousel.min.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="/js/gmaps.min.js"></script>
	<script src="/js/main.js"></script>

</body>

</html>
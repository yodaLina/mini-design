Create table utilisateurs(
  ID integer primary key NOT NULL AUTO_INCREMENT,
  name varchar(50),
  password varchar(100)
);
alter table utilisateurs add constraint unique(name);
Create table categorie(
	ID integer primary key NOT NULL AUTO_INCREMENT,
	name varchar(50),UNIQUE(name)
) ENGINE=InnoDB;
Insert into categorie(name) values('Automne');
Insert into categorie(name) values('Hiver');
Insert into categorie(name) values('Printemps');

Create table destination(
	ID integer primary key NOT NULL AUTO_INCREMENT,
	name varchar(50),UNIQUE(Name)
)ENGINE=InnoDB;
Insert into destination(name) values('Adulte');
Insert into destination(name) values('Enfant');
Insert into destination(name) values('Bébé');

Create table kiraro(
	ID integer primary key NOT NULL AUTO_INCREMENT,
	name varchar(50),UNIQUE(Name),
	price double(6,2),
	idcategorie integer,foreign key(idcategorie) references categorie(ID),
	iddestination integer,foreign key(iddestination) references destination(ID),
	shortdescri varchar(800),
	fulldescri varchar(3000),
	image1 varchar(300),
	image2 varchar(400),
	image3 varchar(500),
	pointure integer,
	couleur varchar(50)
)ENGINE=InnoDB;
Create view kirarodescri as Select kiraro.*,categorie.name as categname,destination.name as destname from kiraro join categorie on kiraro.idcategorie=categorie.id join destination on kiraro.iddestination=destination.id;

delimiter //
    Create function count4Categs(idcateg int) 
	RETURNS int deterministic
	BEGIN
			DECLARE v int;
			Select count(*) into v  from kiraro where IDcategorie=idcateg;
			RETURN(v);
	END
	//

Create view categcount as Select categorie.*,count4Categs(ID) as isany from categorie;

INSERT INTO `kiraro` (`ID`, `name`, `price`, `idcategorie`, `iddestination`, `shortdescri`, `fulldescri`, `image1`, `image2`, `image3`, `pointure`, `couleur`) VALUES (NULL, 'Adidas New Hammer', '150', '1', '1', 'Chaussure d\'athlétisme adidas adizero Discus Hammer 2 : pour un lancer toujours plus loin.', '
La chaussure d\'athlétisme adidas adizero Discus Hammer 2 pour homme est conçue pour la pratique du lancer de marteau et de disque avec une semelle spéciale rotation. Elle est ultra-légère et offre une bonne stabilité ainsi qu\'un excellent confort.
La chaussure adidas adizero Discus Hammer 2 présente une languette en mesh laissant circuler l\'air pour une très bonne ventilation de votre pied.
Le bandeau velcro lui confère un maintien de haute qualité nécessaire à vos performances sur la piste.', 'adidas-new-hammer-princ.jpg', 'adidas-new-hammer-2.jpg', 'adidas-new-hammer-3.jpg', '33', 'gris');